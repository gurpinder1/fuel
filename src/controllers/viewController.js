
const { body, validationResult } = require('express-validator');
const { insertUser, loginUser, getAllUser } = require('../models/userModel');
const { insertQuote,getHistory, getQuotes } = require('../models/quoteModel');
const { insertCustomers, getCustomer} = require('../models/customerModel');

// Display home page
exports.getIndex = (req, res, next) => {
    res.status(200).render('index', {
        title: `Super Fuel | Premium Fuel Delivered in a Click`
    });
}

// Handle user sign-up form on POST
exports.signUp_post = [
    body('email', 'Email must be valid').trim().isEmail().normalizeEmail().escape(),
    body('password')
        .isStrongPassword({
            minLength: 8,
            minLowercase: 1,
            minUppercase: 1,
            minNumbers: 1,
            minSymbols: 1,
        }),
    body('confirm_password').custom((value, { req }) => {
        if (value !== req.body.password) {
            throw new Error('Password confirmation does not match password');
        }
        return true;
    }),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(400).render('index', { title: 'Super Fuel', user: req.body, signUpErrors: errors.array() });
            return;
        }
        else {
            const callback = (user) => {
                console.log('user:', user);
                if (user) {
                    res.redirect('/user/settings');
                }
            };
            insertUser({
                email: req.body.email,
                password: req.body.password,
                callback
            });
        }
    }
];

// Handle user log-in form on POST
exports.login_post = [
    body('email', 'Email is required').trim().isEmail().normalizeEmail().escape(),
    body('password', 'Password is required').trim().isLength({ min: 1 }),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(400).render('index', { title: 'Super Fuel', user: req.body, loginErrors: errors.array() });
            return;
        }
        else {
            const callback = ({ user, token, error }) => {
                if ( token ) {
                    // console.log( 'user:', user );
                    req.session.user = user;
                    res.status( 200 ).json({ token :token,users:user});
                }
                else if (error) {
                    res.status(400).render('index', { title: 'Super Fuel', user: req.body, loginErrors: [{ msg: 'There was no match for your credentials.' }] });
                }
            };
            loginUser({
                email: req.body.email,
                password: req.body.password,
                callback
            });
        }
    }
];

// Handle quote request form on GET
exports.quote_request_get = function (req, res) {
    const sessionUser = req.session.user;
    if ( sessionUser ) {
        console.log("user",sessionUser)
        // res.render('request_fuel_quote', { title: 'Request a quote', user: sessionUser } );

        const callback = ({ history }) => {
           
                res.status(400).render( 'request_fuel_quote', { title: 'Quote', history, user: sessionUser } );
           
        };
        getHistory({ user: sessionUser, callback })
    }
    else {
        res.redirect( '/dashboard' );
    }
};

// Handle quote request form on POST
exports.quote_request_post = [
    body('gallons').trim().isNumeric().withMessage('Gallon amount must be valid')
        .isFloat({ min: 0.5 }).withMessage('Gallon amount must be valid').escape(),
    body('delivery_date').isISO8601().toDate().withMessage('Delivery date must be valid')
        .isAfter().withMessage('Delivery date must be in future').escape(),
    body('suggested_price', 'Suggested price must be valid').isNumeric().escape(),
    body('amount_due', 'Amount due must be valid').isNumeric().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(400).render( 'request_fuel_quote', { title: 'Request Fuel', quote: req.body, errors: errors.array() });
            return;
        }
        else {
            const callback = ({ user }) => {
                if ( user ) {
                    res.redirect( '/quotes' );
                }
            };
            insertQuote({
                gallons: req.body.gallons,
                delivery_date: req.body.delivery_date,
                suggested_price: req.body.suggested_price,
                amount_due: req.body.suggested_price*req.body.gallons,
                token: req.token,
                callback
            });
        }
    }
];



// Handle customer request form on GET
exports.customers_request_get = function (req, res) {
    const sessionUser = req.session.user;
    if ( sessionUser ) {
        res.render('customer', { title: 'Request a customer', user: sessionUser } );
    }
    else {
        res.redirect( '/' );
    }
};

// Handle customer request form on POST
exports.customers_request_post = [
    body('first_name', 'First name must be valid').trim().isLength({ min: 1, max: 50 }).escape(),
    body('last_name', 'Last name must be valid').trim().isLength({ min: 1, max: 50 }).escape(),
    body('company', 'Company Must be valid').trim().isLength({ min:1, max:100}).escape(),
    body('email', 'Email Must be valid').trim().isLength({ min:1, max:100}).escape(),
    body('phone', 'Phone number Must be valid').trim().isNumeric().escape(),
    body('address1', 'Address must be valid').trim().isLength({ min: 1, max: 100 }).escape(),
    body('address2', 'Address must be valid').trim().optional({ checkFalsy: true }).trim().isLength({ max: 100 }).escape(),
    body('city', 'City must be valid').trim().isLength({ min: 1, max: 100 }).escape(),
    body('state', 'State must be valid').trim().isLength({ min: 1, max: 2 }).escape(),
    body('zip_code', 'Zip code must be valid').trim().isLength({ min: 5, max: 9 }).isPostalCode('US').isNumeric().escape(),
    body('country', 'Country must be valid').trim().isLength({ min: 1, max: 100 }).escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(400).render( 'customer', { title: 'Request customer', cust: req.body, errors: errors.array() });
            return;
        }
        else {
            const callback = ({ user }) => {
                if ( user ) {
                    res.redirect( '/customers' );
                }
            };
            insertCustomers({

                first_name: req.body.first_name,
                last_name: req.body.last_name,
                company: req.body.company,
                email: req.body.email,
                phone: req.body.phone,
                address1: req.body.address1,
                address2: req.body.address2,
                city: req.body.city,
                state: req.body.state,
                zip_code: req.body.zip_code,
                country: req.body.country,
                token: req.token,
                callback
            });
        }
    }
];

// Display user log-in form on GET
exports.dashboard_get = function (req, res) {
    getAllUser();

    res.render('dashboard');
};


exports.UserDashboard_get = function (req, res) {
    getAllUser();

    res.render('UserDashboard');
};
// Display customers management page on GET
exports.customers_get = function (req, res) {
    const sessionUser = req.session.user;
    if ( sessionUser ) {
        const callback = ({ customers }) => {
            if ( customers ) {
                res.status( 200 ).render( 'customers', { title: 'Customer', customers, user: sessionUser } );
            }
        };
        getCustomer({ user: sessionUser, token: req.token, callback });
    }
    else {
        res.redirect( '/' );
    }

    // res.render('customers');
};


// Display quotes management page on GET
exports.quotes_get = function (req, res) {
    const sessionUser = req.session.user;
    if ( sessionUser ) {
        const callback = ({ quotes }) => {
            if ( quotes ) {
                res.status( 200 ).render( 'quotes', { title: 'Quote', quotes, user: sessionUser } );
            }
        };
        getQuotes({ user: sessionUser,token:req.token, callback });
    }
    else {
        res.redirect( '/' );
    }
};

// Display invoices management page on GET
exports.invoices_get = function (req, res) {
    res.render('invoices');
};

// Display payments management page on GET
exports.payments_get = function (req, res) {
    res.render('payments');
};

// Display users management page on GET
exports.users_get = function (req, res) {
    res.render('users');
};