
const connection = require( '../dbconfig' );
const { verifyToken } = require('../models/userModel');
const jwt = require( 'jsonwebtoken' );


exports.insertQuote = async ({
    gallons,
    delivery_date,
    suggested_price,
    amount_due,
    token,
    callback 
}) => {
    const decoded = verifyToken( token );
    console.log("rate",amount_due)
    const values = { ClientInformation_ID: decoded.id, gallons, delivery_date, suggested_price, amount_due,quote_status:"pending" };
    connection.query( 'INSERT INTO Fuel_Quote SET ?', values, ( error, results, fields ) => {
        if ( error ) {
            return connection.rollback( () => {
                throw error;
            });
        }
        console.log( 'Successfully inserted new quote!' );
        callback({
            user: decoded
        });
    });
};

exports.getQuotes = async ({
    user,
    token,
    callback 
}) => {
    const decoded = verifyToken( token );
    // if(decoded.type ===1 ||decoded.type ==="1" )
    // {
    //     return;
        
    // }
    // else{
    const sql = `
        SELECT *
        FROM Fuel_Quote
        WHERE
            ClientInformation_ID = ?
    `;
    connection.query( sql, user.ID, ( error, results, fields ) => {
        if ( error ) {
            return connection.rollback( () => {
                throw error;
            });
        }
        console.log( 'results:', results );
        callback({
            quotes: results
        });
    });
// }
};
exports.getHistory = async ({
    user,
    callback 
}) => {
  //  const decoded = verifyToken( token );
    // if(decoded.type ===1 ||decoded.type ==="1" )
    // {
    //     return;
        
    // }
    // else{
    const sql = `
        SELECT *
        FROM customers
        WHERE
            ClientInformation_ID = ?
    `;
    connection.query( sql, user.ID, ( error, results, fields ) => {
        if ( error ) {
            return connection.rollback( () => {
                throw error;
            });
        }
        console.log( 'results:', results );
const result =  results[ 0 ];var history='';
if(!result)
{
history={
    status:0
}
}
else{
    history={
        status:1
    }
}

        callback({
            history:history
        });
    });
// }
};
