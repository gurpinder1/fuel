const connection = require( '../dbconfig' );
const { verifyToken } = require('../models/userModel');

exports.updateUser = async({ first_name, last_name, address1, address2, city, state, zip_code, token, callback }) => {
	const decoded = verifyToken( token );
	const sql = `
		UPDATE ClientInformation
		SET 
			first_name = ?,
			last_name = ?,
			address1 = ?,
			address2 = ?,
			city = ?, 
			state = ?,
			zip_code = ?
		WHERE ID = ?
	`;
	connection.query( sql, [ first_name, last_name, address1, address2, city, state, zip_code, decoded.id ], ( error, results, fields ) => {
		if ( error ) {
			return connection.rollback( ( ) => {
				throw error;
			});
		}
		console.log( 'Successfully updated user settings' );
		const user = {
			ID: decoded.id,
			email: decoded.email,
			first_name,
			last_name,
			address1,
			address2,
			city,
			state,
			zip_code
		}
		callback( user );
	});
};
